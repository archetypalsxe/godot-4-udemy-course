# Section 3 - Pre Flight

## 17 - Assets

* https://www.kenney.nl/
  * Also does have sound
* https://opengameart.org/content/512-sound-effects-8-bit-style
* https://www.glitchthegame.com/public-domain-game-art/
* https://ansimuz.itch.io/sunny-land-pixel-game-art
* https://pixelfrog-assets.itch.io/pixel-adventure-1
* https://opengameart.org/content/parallax-2d-backgrounds
* https://craftpix.net/freebies/free-cartoon-parallax-2d-backgrounds/?utm_campaign=Website&utm_source=opengameart.org&utm_medium=public

## 18 - Games are Too Small

* Project Settings
* Advanced
* Window
* Allow hiDPI -> Uncheck

## 19 - Short Message

* You can email bluefeversoft@gmail.com if you need help or have an error

## 20 - Two Tappy Sections

* Slow or Quick Version
  * Doesn't give quite as much detail
