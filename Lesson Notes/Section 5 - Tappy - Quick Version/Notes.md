# Tappy - The Quick Version

## 55 - Project Setup

* `.import` files
  * Settings for importing
* `.editor` file
  * Keeps track of everything you do in the editor to restore settings and place
* Embedded resource
  * Resource settings/data are stored in the scene file
* Resources from File
  * Settings/data are not stored in the scene file
  * Settings can be reused in different scenes
  * Background is a resource from a file

## 56 - Plane Scene

* Physics2D Nodes
  * StaticBody2D
    * Static, only moves by script
    * Used for floors/walls
  * CharacterBody2D
    * Used for platform characters
    * Can collide with other bodies
    * Moves with velocity in x/y
  * AnimatableBody2D
    * Used for moving platforms and doors
    * StaticBody that moves
  * RigidBody2D
    * Moved by 2D physics simulation
      * Not moved by velocity
    * Objects that have gravity that can be pushed by other objects
    * You don't actually change the location
      * You just provide the force and direction
* When you have multiple images (sprites) in one...
  * It's called a sprite sheet
  * Enable region
  * Edit the region
* Local to Scene in the Collision Shape 2D
  * Make scene "editable children" in the parent scene
  * Allows you to make changes to changes to just one scene when there are duplicates
    * Can be different from the main

## 57 - Flight
* Script functions
  * `_process`
    * Called in main loop
  * `_physics_process`
    * Called in main loop, once per frame, regular interval

## 58 - Animation Player
* Bezier Curves
  * More fine tuned transition to values
  * Rather than just being completely linear

# 59 - Pipes Scene
* Using an Area 2D
  * Simpler than creating a collision body
  * Doesn't have physics
* VisibleOnScreenNotifier2D
  * Used to remove objects when they go off the screen
  * Signals used for things like `body_entered` and `body_exited`
    * Using the `screen_exited` function on the VisibleOnScreenNotifier2D object
  * `queue_free()` schedules the object to be removed from the game
* Using groups to identify what various objects are
  * Otherwise we were getting collisions from the screen boundary... boundaries

# 60 - Spawning

* Marker2D used to determine the range that the pipes will spawn

# 331 - Autoload (Singleton)

* Storing constants
* To have the game autoload a constant file:
  * > Project
  * Project Settings
  * Autoload
* To access:
  * GameManager.SCROLL_SPEED

## Event Hub / Signal Bus

* Method for having events trigger stuff
* Doing it with having scenes emit their own event becomes unwiedly at scale
* All the signals are handled by a singleton
  * Rather than having the signal emitted by each object

# 332 - Scoring

* Shouldn't try and mess with manual positioning
  * Use a container instead and have a border on it
* CanvasLayer relies on layer ordering rather than positioning or z index

# 333 - Main Scene

* Mainly using container sizing horizontal/vertical to place things
  * Under Layout > Container Sizing
  * Combined with horizontal/vertical alignment
* Game manager used to change the scene
  * Used with PackedScene resources

# 334 - Game Over

* Using a "Timer" resource to switch between Game Over and Press Space
