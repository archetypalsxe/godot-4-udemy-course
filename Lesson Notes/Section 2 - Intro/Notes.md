# Section 2 - Introduction to Godot

* **When switching from one operating system to another... does not need to rebuild/modify**
  * At least from what I have seen so far which is quite limited
  * Also... may have changed in Unity since I last used it

## 3 Setup and Test

* Renderer options
  * Forward+ is for more advanced graphic desktop games
* Will be using mobile throughout the course
  * Work on mobile phones
* Compatibility
  * Used if your computer does not support the Vulcan engine
  * Uses OpenGL

## 4 Scenes

* **Using GDScript in this course**
* x coordinates are positive going right
* **y coordinates are positive going down**
* **For hiDPI... may be an issue with retina display**
  * Project > Project Settings > Advanced Settings > Display | Window > Allow hiDPI -> Off
* **[Keyboard Mapping](https://docs.godotengine.org/en/stable/tutorials/editor/default_key_mapping.html)**

## 6 Multiple Scenes

* When you make a node a child of another...
  * It's transform becomes relative to the parent node
  * Moving the parent also moves the child
    * Position does not change
      * It's the same, relative to the parent
    * Moving the child, does not impact the parent
* Global positioning is also used
  * Not viewable in the UI
  * Only available in the code/scripts
* Nodes are even used to animate other nodes
  * `AnimationPlayer` child node
* Child scenes are added to parent scene to make games

## 7 Creating a Script

* Only able to attach one script to a node
* Script functions
  * `_ready` function is called when the node is ready in the scene tree
  * `_process(delta)`
    * Called in the main loop
    * Called every frame, as fast as possible
    * **Delta is the elapsed time in seconds since the last time it was called**
      * Is a float
    * UI is generally put in here because it doesn't matter if it's processed at a regular interval
  * `_physics_process(delta)`
    * Called in the main loop once per frame
    * Every frame, but regular interval
    * Physics calculations are done in this function
* `print("Print statement")`
  * Used to echo to the console
  * `print("Delta: ", delta)`

## 8 - Variables

* `var score: int = 25`
  * `String`
  * `float`
  * `bool`
  * `null`
    * Not actually a type
* **Dynamically typed**
  * `var score = 25`
* `score = 30`
  * Variable reassignment
* Formatted String
  * `var fs: String = "Speed: %s" % speed` # Formatted as a string
  * `var fs: String = "Speed: %.1f" % speed` # Formatting number with 1 decimal place
  * `print("%s %s %.2f %s" % [name, score, speed, dead])`

## 9 - Operators
* `x += 5`
  * `*=`
  * `/=`
  * `%=`
* `var is_motivated: bool = is_alive and is_hungry`
  * `or`
  * `!hungry`
```
if age >= 18 and has_license:
  print("You are eligible to drive")
elif age >= 25:
  print("Something")
else:
  print("Something else")
```

Switch statement:
```
match(enemy):
  "Dragon":
    print("It's a dragon")
  "Wizard":
    print("Wizzzzzzzzard")
  _: # Default
    print("Default")
```
## 10 - Arrays and Loops

* `var fruits: Array = ["apple", "banana"]`
* `print("Fruit: ", fruits[0])`
* `print("Last fruit: ", fruits[-1])`
* `fruits[1] = "kiwi"`
* `fruits.size()`
* `fruits.append("pear")`
* `Array.shuffle()`
* `Array.pick_random()`

For loop
```
for fruit in fruits:
  print(fruit)
```

```
# range(10) does the same if the starting number is 0
# range(0,10,2) will count in 2s
for number in range(0,10): # Does not include 10
  print(number)
```

```
var count: int = 0
while count < 5:
  count += 1
```

## 11 - Dictionaries

### Defining
```
var person: Dictionary = {
  "name": "John",
  "age": 25
}
person.age = 30
print("Age: ", person["age"])
print("person: ", person)
var num_keys = person.size()
person["job"] = "engineer"
var person_has_job = "job" in person
for key in person:
  print(key)
```

## 12 - Functions

```
say_hello("Fred")
func say_hello(player_name: String) -> void:
  print("Hello, " + player_name + ", " welcome to the course!")

func add_num(a: int, b: int) -> int:
  return a + b
```

Blue arror in Godot UI means that we're overwriting a function

## 13 - Classes

* Sprites are classes
* Strings are classes

animal.gd
```
class_name Animal

var _my_age: int = 12

func say_age() -> void:
  print(_my_age)
```

main.gd
```
class_name Main # Allows other classes to be referenced/used
var animal = Animal.new()
animal.say_age()
```

main.gd
This is only available locally, in the script that it's declared in
It's called an "inner class"
```
class Player:
  var name: String
```

## 14 - Scripting a Scene

Detecting when the enter key is pressed
```
const DANGER: int = 2
var _lives: int
@export var start_lives: int = 5
enum GAME_STATUS { PLAYING, PAUSED, GAME_OVER }

# Called when the node enters the scene tree for the first time.
func _ready():
	_lives = start_lives
	print(_lives)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_accept") == true:
		reduce_lives()

func reduce_lives() -> void:
	_lives -= 1
	if _lives <= 0:
		print("DEAD")
	elif _lives <= DANGER:
		print("DANGER! " + str(_lives) + " lives remaining!")
	else:
		print("Ouch! " + str(_lives) + " lives remaining")
```

## 15 - Setters and Getters

main.gd
```
var _health: int:
  get = get_health, set = set_health # Setting the setters and getters

func _ready():
  # Calls the functions automatically
  _health = 100 
  print(_health)

func get_health() -> int:
  return _health

func set_health(value: int) -> void:
  _health = value
```

## 16 - Aspect Ratios

* Margin Container
  * Set to "fill rect" in order to utilize the full aspect ratio
* Project Settings
  * Window > Stretch
  * Mode -> `canvas_items`
  * Aspect: `keep`
