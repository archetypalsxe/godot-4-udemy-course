# Godot 4 Udemy Course

This repository is basically me following along to a [Udemy Course](https://www.udemy.com/course/jumpstart-to-2d-game-development-godot-4-for-beginners) that I'm doing on Godot 4. I have a lot of experience in software engineering, some experience (that is rusty and dated) with Unity, but this will be my first time using Godot for what it's worth. Here we go!