extends Node2D

const DANGER: int = 2
const MAX_X_VALUE: int = 1050
var _lives: int
@export var start_lives: int = 5

# Without onready will just set this to null because $Icon2 doesn't exist
@onready var icon_2 = $Icon2

enum GAME_STATUS { PLAYING, PAUSED, GAME_OVER }
var _current_status: GAME_STATUS = GAME_STATUS.PLAYING

# Called when the node enters the scene tree for the first time.
func _ready():
	_lives = start_lives
	print(_lives)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_accept") == true:
		reduce_lives()
	
	if icon_2.position.x < MAX_X_VALUE:
		icon_2.position.x += delta * 50.0
		print(icon_2.position.x)

func reduce_lives() -> void:
	_lives -= 1
	if _lives <= 0:
		print("DEAD")
	elif _lives <= DANGER:
		print("DANGER! " + str(_lives) + " lives remaining!")
	else:
		print("Ouch! " + str(_lives) + " lives remaining")
