extends Node

const DEFAULT_PIPE_POINTS: int = 1

var _score: int = 0
var _high_score: int = 0

func get_score() -> int:
	return _score

func get_high_score() -> int:
	return _high_score

func set_score(new_score: int) -> void:
	_score = new_score
	_set_high_score()
	SignalManager.on_score_updated.emit()

func increment_score(delta: int = DEFAULT_PIPE_POINTS) -> void:
	set_score(_score + delta)

func _set_high_score() -> void:
	if _score > _high_score:
		_high_score = _score
