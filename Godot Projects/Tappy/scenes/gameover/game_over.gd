extends Control

@onready var game_over: Label = $GameOver
@onready var space_label: Label = $SpaceLabel
@onready var timer: Timer = $Timer

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	hide()
	SignalManager.on_plane_died.connect(on_plane_died)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if space_label.visible == true:
		if Input.is_action_just_pressed("fly") == true:
			GameManager.load_main_scene()

func on_plane_died() -> void:
	show()
	timer.start()

func _on_timer_timeout() -> void:
	game_over.hide()
	space_label.show()
