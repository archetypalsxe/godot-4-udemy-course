extends CharacterBody2D

@onready var animated_sprite_2d = $AnimatedSprite2D
@onready var animation_player = $AnimationPlayer

const GRAVITY: float = 1500
const POWER: float = -500

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	fly(delta)
	move_and_slide()
	if is_on_floor():
		die()

func fly(delta: float) -> void:
	if Input.is_action_just_pressed("fly") == true:
		velocity.y = POWER
		animation_player.play("power")
	else:
		velocity.y += GRAVITY * delta

func die() -> void:
	SignalManager.on_plane_died.emit()
	set_physics_process(false)
	animated_sprite_2d.stop()
